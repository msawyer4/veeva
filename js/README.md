In order to run, you must install npm and the 'request' module.

Then execute `node weather` to display the last 10 days of precipitation data,
sorted from most to least amount of precipitation.
