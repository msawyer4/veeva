var weatherApp = angular.module('weatherApp', []);

weatherApp.controller('HomeController', ['$http', function($http) {
this.ten_days = [];
this.weather;
var home = this;
    
$http.get('/api/20150206-weather.json').success(
    function(data) {
        home.weather = data;
        console.log(home.weather);
    });
}]);