var request = require('request');
var weather;
var chart = [];
var calendar = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

function compare(a, b) {
    if (a.precipi > b.precipi) {
        return -1;
    } else if (a.precipi < b.precipi) {
        return 1;
    } else {
        return 0;
    }
}

function compare_date(a, b) {
    if (a.date > b.date) {
        return -1;
    } else if (a.date < b.date) {
        return 1;
    } else {
        return 0;
    }
}

function print() {
    var i;
    for (i=0; i<10; i++) {
        console.log(chart[i].precipi +
                    "\t" +
                    chart[i].date);
    }
}

function add(weather) {
    var row = {"precipi":weather.history.dailysummary[0].precipi,
               "date":weather.history.dailysummary[0].date.pretty
        .match(/on.*/)[0].slice(3)};
    chart.push(row);
    if (chart.length == 10) {
        chart.sort(compare_date);
        chart.sort(compare);
        print();
    }
}


var d = new Date();
var year = d.getFullYear().toString();
var month = d.getMonth()+1;
var day = d.getDate();
if (month < 10)
    month = "0" + month.toString();

console.log("Precipitation Totals for Chico, CA:");
console.log("Inches\tDay");

var x;
for (x = 1; x <= 10; x++) {
    if ((day-x) == 0) {
        month--;
        if (month == 0)
            month = 12;
        day = calendar[month-1]+x;
    }
    if ((day-x).toString().length < 2)
        day_p = "0" + (day-x).toString();
    else
        day_p = (day-x).toString();
    if (month.toString().length < 2) {
        month = "0" + month.toString();
    }
    else
        month = month.toString();

    var file = year + month + day_p + "-weather.json";
    request('http://www.michaelasawyer.com/Veeva/'+file,
        function (error, response, body) {
        if (!error && response.statusCode == 200) {
            //console.log(body);
            weather = JSON.parse(body);

            add(weather);
        }
    });
}